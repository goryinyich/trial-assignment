
# wrapper for cpp_calc_pnl from calc_pnl.cpp
calc.pnl <- function(
    weights,
    mdata,
    trading.delay,
    initial.capital,
    min.trade.vol,
    spread.cost,
    fix.fee,
    tvr.fee
) {
    bkts <- cpp_calc_pnl(
        weights = weights,
        close_px = mdata$Close,
        trading_delay = Config$backtester$trading.delay,
        initial_cash = Config$backtester$initial.capital,
        min_trade_volume = Config$backtester$min.trade.vol,
        spread_cost = Config$backtester$spread.cost,
        fix_fee = Config$backtester$fixed.fee,
        tvr_fee = Config$backtester$tvr.fee
    )
    bkts$effective.weights <- timeSeries(bkts$effective.weights, mdata$Timeline, units = mdata$Tickers)
    bkts$capital <- timeSeries(bkts$capital, mdata$Timeline)
    bkts$fees <- timeSeries(bkts$fees, mdata$Timeline)
    bkts$slippage <- timeSeries(bkts$slippage, mdata$Timeline)
    bkts$cash <- timeSeries(bkts$cash, mdata$Timeline)
    bkts$pnl <- bkts$capital / delay(bkts$capital, 1) - 1
    bkts$dead_pos <- timeSeries(bkts$dead_pos, mdata$Timeline)
    bkts
}

# performance stats for pnl
perf.return.annualized <- function(ret, geometric = T) {
  years <- length(ret) / 252
  if (geometric) cagr <- tail(cumprod(1 + ifelse(is.finite(ret), ret, 0)), 1) - 1 else cagr <- sum(ret, na.rm=T)
  if (years > 0.9 & years < 1.1) return(cagr)
  (1 + cagr) ^ (1 / years) - 1
}

perf.ulcer.perf.index <- function(ret, geometric = T) {
  if (geometric) {
    eqy <- cumprod(1 + ifelse(is.finite(ret), ret, 0))
    dds <- 1 - eqy / cummax(eqy)
  } else {
    eqy <- cumsum(ret[is.finite(ret)])
    dds <- cummax(eqy) - eqy
  }
  ulcer <- sd(dds^2, na.rm=T)^0.5
  return(perf.return.annualized(ret, geometric) / ulcer)
}

perf.sharpe.annualized <- function(ret) {
  mean(ret, na.rm=T) / sd(ret, na.rm=T) * sqrt(252)
}

perf.max.dd <- function(ret, geometric = T) {
  if (geometric) {
    eqy <- cumprod(1 + ifelse(is.finite(ret), ret, 0))
    return(max(1 - eqy / cummax(eqy)))
  } else {
    eqy <- cumsum(ret[is.finite(ret)])
    return(max(cummax(eqy) - eqy))
  }
}

perf.avg.dd <- function(ret, geometric = T) {
  if (geometric) {
    eqy <- cumprod(1 + ifelse(is.finite(ret), ret, 0))
    return(mean(1 - eqy / cummax(eqy), na.rm=T))
  } else {
    eqy <- cumsum(ret[is.finite(ret)])
    return(mean(cummax(eqy) - eqy, na.rm=T))
  }
}

perf.calmar <- function(ret, geometric = T) {
  perf.return.annualized(ret, geometric = geometric) / perf.max.dd(ret, geometric = geometric)
}

complete.fields <- function(ret, tvr, geometric, fields) {
  sapply(fields, function(field) {
    if (field == 'ann.ret') return(100 * perf.return.annualized(ret, geometric = geometric))
    if (field == 'sharpe') return(perf.sharpe.annualized(ret))
    if (field == 'calmar') return(perf.calmar(ret, geometric = geometric))
    if (field == 'ulcer') return(perf.ulcer.perf.index(ret, geometric = geometric))
    if (field == 'mdd') return(100 * perf.max.dd(ret, geometric = geometric))
    if (field == 'add') return(100 * perf.avg.dd(ret, geometric = geometric))
    if (field == 'tvr') return(sum(tvr, na.rm=T))
    if (field == 'margin') return(10000 * sum(ret, na.rm=T) / sum(tvr, na.rm=T))
    if (field == 'cumdd05') {
      dds <- -100 * PerformanceAnalytics::findDrawdowns(ret, geometric = geometric)$return
      return(sum(dds[dds > 5]))
    }
  })
}

fetch.performance <- function(
  ret,
  tvr,
  periodicity = 'Y', # M - monthly, Q = quarterly, Y = annual
  # int N - for N-year intervals
  geometric = T,
  fields = c('ann.ret', 'sharpe', 'calmar', 'ulcer', 'mdd', 'add', 'tvr', 'margin')
) {
  times <- as.Date(time(ret))
  years <- as.POSIXlt(times)$year
  if (class(periodicity) == 'character') {
    groups <- switch(periodicity,
                     'M' = 100 * as.POSIXlt(times)$year + as.POSIXlt(times)$mon,
                     'Q' = 10 * as.POSIXlt(times)$year + as.POSIXlt(times)$mon %/% 3,
                     'Y' = as.POSIXlt(times)$year)
  } else {
    groups <- (years - tail(years, 1) %% periodicity - 1) %/% periodicity
  }
  
  result <- data.frame(do.call(rbind, lapply(unique(groups), function(gn) {
    ret.l <- ret[groups == gn, ]
    tvr.l <- tvr[groups == gn, ]
    complete.fields(ret.l, tvr.l, geometric, fields)
  })))
  # for the whole period
  result <- rbind(result,
                  complete.fields(ret, tvr, geometric, fields)
  )
  colnames(result) <- fields
  # add dates as rownames
  rownames(result) <- c(sapply(unique(groups), function(gn) {
    as.character(tail(times[groups == gn], 1))
  }), 'WHOLE')
  return(round(result, 2))
}
