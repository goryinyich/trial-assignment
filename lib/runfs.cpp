#include <Rcpp.h>
#include <cstring>
#include <math.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector run_sum(const NumericVector& v,
                      const int window,
                      const int min_valid = 0) {
    int n = v.size();
    NumericVector result(n);
    int from = 0;
    int count_valid = 0;
    double sum = 0;
    for (int to = 0; to < n; ++to) {
        // add new observation
        if (std::isfinite(v[to])) {
            ++count_valid;
            sum += v[to];
        }
        // delete old observation
        if (to - from == window) {
            if (std::isfinite(v[from])) {
                --count_valid;
                sum -= v[from];
            }
            ++from;
        }
        // evaluate
        result[to] = (count_valid >= min_valid ? sum : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_mean(const NumericVector& v,
                       const int window,
                       const int min_valid = 1) {
    int n = v.size();
    NumericVector result(n);
    int from = 0;
    int count_valid = 0;
    double sum = 0;
    for (int to = 0; to < n; ++to) {
        // add new observation
        if (std::isfinite(v[to])) {
            ++count_valid;
            sum += v[to];
        }
        // delete old observation
        if (to - from == window) {
            if (std::isfinite(v[from])) {
                --count_valid;
                sum -= v[from];
            }
            ++from;
        }
        // evaluate
        result[to] = (count_valid >= min_valid ? sum / count_valid : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_var(const NumericVector& v,
                      const int window,
                      const int min_valid = 2,
                      const bool mean_zero = false,
                      const int extra_df = 0) // internal
{
    const int _min_valid = std::max<int>(min_valid, 2);
    const int n = v.size();
    NumericVector result(n);
    NumericVector mean(n, 0.0);
    if (!mean_zero) {
        mean = run_mean(v, window, min_valid);
    }
    int from = 0;
    int count_valid = 0;
    double sum = 0;
    for (int to = 0; to < n; ++to) {
        // add new observation
        if (std::isfinite(v[to])) {
            ++count_valid;
            sum += v[to] * v[to];
        }
        // delete old observation
        if (to - from == window) {
            if (std::isfinite(v[from])) {
                --count_valid;
                sum -= v[from] * v[from];
            }
            ++from;
        }
        // evaluate
        result[to] = (count_valid >= _min_valid ?
            (sum - count_valid * mean[to] * mean[to]) / (count_valid - 1 + extra_df) : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_sd(const NumericVector& v,
                     const int window,
                     const int min_valid = 2,
                     const bool mean_zero = false,
                     const int extra_df = 0) // internal
{
    NumericVector result = run_var(v, window, min_valid, mean_zero, extra_df);
    for (int i = 0; i < result.size(); ++i) {
        result[i] = sqrt(result[i]);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_min(const NumericVector& v,
                      const int window,
                      const int min_valid = 1) {
    const int n = v.size();
    NumericVector result(n);
    int from = 0;
    double curr_min = NAN;
    int curr_count = 0;
    int count_valid = 0;
    for (int i = 0; i < n; ++i) {
        // add new observation
        if (std::isfinite(v[i])) {
            ++count_valid;
            if (!std::isfinite(curr_min) || v[i] < curr_min) {
                curr_min = v[i];
                curr_count = 1;
            } else if (curr_min == v[i]) {
                ++curr_count;
            }
        }
        // remove old observation
        if (i - from == window) {
            if (std::isfinite(v[from])) {
                --count_valid;
                if (v[from] == curr_min) --curr_count;
                if (curr_count == 0) {
                    // find new min
                    curr_min = NAN;
                    for (int j = from+1; j <= i; ++j) {
                        if (!std::isfinite(v[j])) continue;
                        if (!std::isfinite(curr_min) || v[j] < curr_min) {
                            curr_min = v[j];
                            curr_count = 1;
                        } else if (v[j] == curr_min) {
                            ++curr_count;
                        }
                    }
                }
            }
            ++from;
        }
        // evaluate
        result[i] = (count_valid >= min_valid ? curr_min : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_max(const NumericVector& v,
                      const int window,
                      const int min_valid = 1) {
    return -run_min(-v, window, min_valid);
}

// [[Rcpp::export]]
NumericVector run_argmin(const NumericVector& v,
                         const int window,
                         const int min_valid = 1) {
    const int n = v.size();
    NumericVector result(n);
    int from = 0;
    double curr_min = NAN;
    int curr_count = 0;
    int count_valid = 0;
    int last_position = 0;
    for (int i = 0; i < n; ++i) {
        // add new observation
        if (std::isfinite(v[i])) {
            ++count_valid;
            if (!std::isfinite(curr_min) || v[i] < curr_min) {
                curr_min = v[i];
                curr_count = 1;
                last_position = i;
            } else if (curr_min == v[i]) {
                ++curr_count;
                last_position = i;
            }
        }
        // remove old observation
        if (i - from == window) {
            if (std::isfinite(v[from])) {
                --count_valid;
                if (v[from] == curr_min) --curr_count;
                if (curr_count == 0) {
                    // find new min
                    curr_min = NAN;
                    for (int j = from+1; j <= i; ++j) {
                        if (!std::isfinite(v[j])) continue;
                        if (!std::isfinite(curr_min) || v[j] < curr_min) {
                            curr_min = v[j];
                            curr_count = 1;
                            last_position = j;
                        } else if (v[j] == curr_min) {
                            ++curr_count;
                            last_position = j;
                        }
                    }
                }
            }
            ++from;
        }
        // evaluate
        result[i] = (count_valid >= min_valid ? i - last_position : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_argmax(const NumericVector& v,
                         const int window,
                         const int min_valid = 1) {
    return run_argmin(-v, window, min_valid);
}

inline double smallest_set_element(const std::multiset<double>& s) {
    if (s.size() == 0) return NAN;
    return (*s.begin());
}
inline double pick_smallest_set_element(std::multiset<double>& s) {
    if (s.size() == 0) return NAN;
    double result = *s.begin();
    s.erase(s.begin());
    return result;
}

inline double largest_set_element(const std::multiset<double>& s) {
    if (s.size() == 0) return NAN;
    return(*(--(s.end())));
}
inline double pick_largest_set_element(std::multiset<double>& s) {
    if (s.size() == 0) return NAN;
    std::multiset<double>::iterator i = --(s.end());
    double result = *i;
    s.erase(i);
    return result;
}

// [[Rcpp::export]]
NumericVector run_quantile(const NumericVector v,
                           const int window,
                           const double q,
                           const int min_valid = 1) {
    std::multiset<double> lower;
    std::multiset<double> upper;
    NumericVector result(v.size());
    int from = 0;
    for (int i = 0; i < v.size(); ++i) {
        // add new element
        if (std::isfinite(v[i])) {
            if (upper.size() == 0 || v[i] < smallest_set_element(upper)) lower.insert(v[i]);
            else upper.insert(v[i]);
        }
        // remove old element
        if (i - from == window) {
            if (std::isfinite(v[from])) {
                std::multiset<double>::const_iterator i = lower.find(v[from]);
                if (i != lower.end()) lower.erase(i);
                else upper.erase(upper.find(v[from]));
            }
            ++from;
        }
        // redistribute
        int count_valid = lower.size() + upper.size();
        while (lower.size() > 0 && q <= (lower.size() - 0.5) / count_valid) {
            upper.insert(pick_largest_set_element(lower));
        }
        while (upper.size() > 0 && q >= (lower.size() + 0.5) / count_valid) {
            lower.insert(pick_smallest_set_element(upper));
        }
        // evaluate
        if (count_valid < min_valid) result[i] = NAN;
        else {
            if (lower.size() == 0) result[i] = smallest_set_element(upper);
            else if (upper.size() == 0) result[i] = largest_set_element(lower);
            else {
                double r = q * count_valid - (lower.size() - 0.5);
                result[i] = (1.0 - r) * largest_set_element(lower) + r * smallest_set_element(upper);
            }
        }
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_median(const NumericVector v,
                         const int window,
                         const int min_valid = 1) {
    return run_quantile(v, window, 0.5, min_valid);
}

// [[Rcpp::export]]
NumericVector run_cov(const NumericVector x,
                      const NumericVector y,
                      const int window,
                      const int min_valid = 2,
                      const bool mean_zero_x = false,
                      const bool mean_zero_y = false,
                      const int extra_df = 0) { // internal
    int n = x.size();
    NumericVector mean_x(n, 0.0), mean_y(n, 0.0);
    if (!mean_zero_x) {
        mean_x = run_mean(x, window, min_valid);
    }
    if (!mean_zero_y) {
        mean_y = run_mean(y, window, min_valid);
    }
    NumericVector result(n);
    int from = 0;
    int count_valid = 0;
    double sum_x = 0.0, sum_y = 0.0, sum_xy = 0.0;
    for (int i = 0; i < n; ++i) {
        // add new observation
        if (std::isfinite(x[i]) && std::isfinite(y[i])) {
            ++count_valid;
            sum_x += x[i];
            sum_y += y[i];
            sum_xy += x[i] * y[i];
        }
        // delete old observation
        if (i - from == window) {
            if (std::isfinite(x[from]) && std::isfinite(y[from])) {
                --count_valid;
                sum_x -= x[from];
                sum_y -= y[from];
                sum_xy -= x[from] * y[from];
            }
            ++from;
        }
        // evaluate
        result[i] = (count_valid >= min_valid ? (sum_xy - sum_x * mean_y[i] - sum_y * mean_x[i] +
                     count_valid * mean_x[i] * mean_y[i]) / (count_valid - 1 + extra_df) : NAN);
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_cor(const NumericVector x,
                      const NumericVector y,
                      const int window,
                      const int min_valid = 3,
                      const bool mean_zero_x = false,
                      const bool mean_zero_y = false) {
    // filter only pairwise complete
    NumericVector x_ = clone<NumericVector>(x);
    NumericVector y_ = clone<NumericVector>(y);
    for (int i = 0; i < x_.size(); ++i) {
        if (!std::isfinite(x_[i]) || !std::isfinite(y_[i])) {
            x_[i] = NAN;
            y_[i] = NAN;
        }
    }
    NumericVector result = run_cov(x_, y_, window, min_valid, mean_zero_x, mean_zero_y, 1);
    NumericVector sd_x = run_sd(x_, window, min_valid, mean_zero_x, 1);
    NumericVector sd_y = run_sd(y_, window, min_valid, mean_zero_y, 1);
    for (int i = 0; i < result.size(); ++i) {
        if (result[i] == 0) continue;
        if (sd_x[i] <= 0 || sd_y[i] <= 0) result[i] = NAN;
        else result[i] = result[i] / sd_x[i] / sd_y[i];
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_ls_slope(const NumericVector x,
                           const NumericVector y,
                           const int window,
                           const int min_valid = 2) {
  NumericVector sd_x = run_sd(x, window, min_valid, false, 1);
  for (int i = 0; i < sd_x.size(); ++i) if (sd_x[i] <= 0) sd_x[i] = NAN;
  NumericVector result = (run_mean(x * y, window, min_valid) -
    run_mean(x, window, min_valid) * run_mean(y, window, min_valid)) / pow(sd_x, 2.0);
  return result;
}

// [[Rcpp::export]]
NumericVector run_ls_intercept(const NumericVector x,
                               const NumericVector y,
                               const int window,
                               const int min_valid = 2) {
  NumericVector result = run_mean(y, window, min_valid) -
    run_ls_slope(x, y, window, min_valid) * run_mean(x, window, min_valid);
  return result;
}

// [[Rcpp::export]]
NumericVector run_ls_r2(const NumericVector x,
                        const NumericVector y,
                        const int window,
                        const int min_valid = 2) {
  // Here we use the fact that R^2 of simple OLS exactly equals to cor(x, y)^2
  NumericVector result = run_cor(x, y, window, min_valid);
  return result * result;
}

// [[Rcpp::export]]
NumericVector run_smooth(const NumericVector x,
                         const NumericVector kernel,
                         const int min_valid = 1,
                         const bool keep_na = false) {
    int n = x.size();
    int m = kernel.size();
    NumericVector result(n, NAN);
    int from = 0;
    int count_valid = 0;
    for (int i = 0; i < n; ++i) {
        // add point
        if (std::isfinite(x[i])) ++count_valid;
        // remove point
        if (i - from == m) {
            if (std::isfinite(x[from])) {
                --count_valid;
                ++from;
            }
        }
        // evaluate integral
        if (count_valid >= min_valid && (std::isfinite(x[i]) || !keep_na)) {
            double sum_wt = 0.0;
            double integral = 0.0;
            int to = std::min<int>(m, i + 1);
            for (int j = 0; j < to; ++j) {
                if (std::isfinite(x[i - j])) {
                    sum_wt += fabs(kernel[j]);
                    integral += x[i - j] * kernel[j];
                }
            }
            if (sum_wt > 0) result[i] = integral / sum_wt;
        }
    }
    return result;
}

// [[Rcpp::export]]
NumericVector run_zscore(const NumericVector& v,
                         const int window,
                         const double min_value = -3.0,
                         const double max_value = 3.0,
                         const int min_valid = 2) {
    NumericVector result = v - run_mean(v, window, min_valid);
    NumericVector sd = run_sd(v, window, min_valid);
    for (int i = 0; i < sd.size(); ++i) {
        if (std::isfinite(sd[i]) && sd[i] > 0) {
            result[i] /= sd[i];
            if (std::isfinite(result[i])) {
                result[i] = std::min<double>(max_value, std::max<double>(min_value, result[i]));
            }
        }
    }
    return result;
}
// [[Rcpp::export]]
NumericVector run_prod(const NumericVector& v,
                       const int window,
                       const int min_valid = 0) {
  // implemented as exp(run_sum(log(v))), with separate
  // treatment of non-positive numbers
  int n = v.size();
  NumericVector is_negative(n, 0.0);
  NumericVector is_zero(n, 0.0);
  for (int i = 0; i < n; ++i) {
    if (std::isfinite(v[i]) && window > 0) {
      if (v[i] == 0) is_zero[i] = 1;
      if (v[i] < 0) is_negative[i] = 1;
    }
  }
  if (window > 0) {
    is_negative = run_sum(is_negative, window);
    is_zero = run_max(is_zero, window);
  }
  NumericVector log_sum = run_sum(log(abs(v)), window, min_valid);
  NumericVector result(n);
  int from = 0;
  int count_valid = 0;
  for (int to = 0; to < n; ++to) {
    // add new observation
    if (std::isfinite(v[to])) ++count_valid;
    // delete old observation
    if (to - from == window) {
      if (std::isfinite(v[from])) --count_valid;
      ++from;
    }
    // evaluate
    double val = exp(log_sum[to]);
    if (int(is_negative[to]) % 2 == 1) val *= -1.0;
    if (is_zero[to]) val = 0.0;
    result[to] = (count_valid >= min_valid ? val : NAN);
  }
  return result;
}

// [[Rcpp::export]]
NumericVector run_ema(const NumericVector& v,
                      const double period,
                      const int window = 0,
                      const int min_valid = 1,
                      const bool keep_na = false) {
  if (period < 1.0) Rcpp::stop("period >= 1 is FALSE");
  const int eff_window = (window == 0 ? int(round(6 * period)) : window);
  double lambda = 1.0 - 1.0 / period;
  NumericVector kernel(eff_window);
  kernel[0] = 1;
  for (int i = 1; i < eff_window; ++i) kernel[i] = lambda * kernel[i - 1];
  return run_smooth(v, kernel, min_valid, keep_na);
}

// [[Rcpp::export]]
List run_cov_mat(const NumericMatrix& x,
                 const int window,
                 const int min_valid = 2,
                 const std::string& use = "complete.obs",
                 const bool mean_zero = false) {
  if (use != "complete.obs") {
    Rcpp::stop("Invalid value for \"use\" parameter (look at the documentation for possible values)");
  }
  NumericMatrix isvalid = NumericMatrix(x.nrow(), x.ncol());
  for (int i = 0; i < x.nrow(); ++i) {
    for (int j = 0; j < x.ncol(); ++j) {
      isvalid(i, j) = std::isfinite(x(i, j)) ? 1.0 : 0.0;
    }
  }
  for (int i = 0; i < x.ncol(); ++i) {
    isvalid(_, i) = run_sum(isvalid(_, i), window);
  }
  // pairwise covs
  std::vector<std::vector<int> > nums = std::vector<std::vector<int> >(x.ncol(), std::vector<int>(x.ncol()));
  std::vector<NumericVector> run_covs;
  for (int i = 0; i < x.ncol(); ++i) {
    for (int j = i; j < x.ncol(); ++j) {
      run_covs.push_back(run_cov(x(_, i), x(_, j), window, window, mean_zero, mean_zero));
      nums[i][j] = nums[j][i] = run_covs.size() - 1;
    }
  }
  std::vector<List> result;
  for (int i = 0; i < x.nrow(); ++i) {
    // valid indices
    std::vector<int> valid_ix;
    for (int j = 0; j < x.ncol(); ++j) if (int(isvalid(i, j)) == window) valid_ix.push_back(j + 1);
    NumericMatrix cov_mat = NumericMatrix(valid_ix.size(), valid_ix.size());
    for (int j = 0; j < valid_ix.size(); ++j) {
      for (int k = 0; k < valid_ix.size(); ++k) {
        cov_mat(j, k) = run_covs[nums[valid_ix[j] - 1][valid_ix[k] - 1]][i];
      }
    }
    result.push_back(
      List::create(
        Named("valid.ids") = valid_ix,
        Named("cov.mat") = cov_mat
      )
    );
  }
  return wrap(result);
}


// [[Rcpp::export]]
List run_cor_mat(const NumericMatrix& x,
                 const int window,
                 const int min_valid = 2,
                 const std::string& use = "complete.obs",
                 const bool mean_zero = false) {
  if (use != "complete.obs") {
    Rcpp::stop("Invalid value for \"use\" parameter (look at the documentation for possible values)");
  }
  NumericMatrix isvalid = NumericMatrix(x.nrow(), x.ncol());
  for (int i = 0; i < x.nrow(); ++i) {
    for (int j = 0; j < x.ncol(); ++j) {
      isvalid(i, j) = std::isfinite(x(i, j)) ? 1.0 : 0.0;
    }
  }
  for (int i = 0; i < x.ncol(); ++i) {
    isvalid(_, i) = run_sum(isvalid(_, i), window);
  }
  // pairwise covs
  std::vector<std::vector<int> > nums = std::vector<std::vector<int> >(x.ncol(), std::vector<int>(x.ncol()));
  std::vector<NumericVector> run_cors;
  for (int i = 0; i < x.ncol(); ++i) {
    for (int j = i; j < x.ncol(); ++j) {
      run_cors.push_back(run_cor(x(_, i), x(_, j), window, window, mean_zero, mean_zero));
      nums[i][j] = nums[j][i] = run_cors.size() - 1;
    }
  }
  std::vector<List> result;
  for (int i = 0; i < x.nrow(); ++i) {
    // valid indices
    std::vector<int> valid_ix;
    for (int j = 0; j < x.ncol(); ++j) if (int(isvalid(i, j)) == window) valid_ix.push_back(j + 1);
    NumericMatrix cor_mat = NumericMatrix(valid_ix.size(), valid_ix.size());
    for (int j = 0; j < valid_ix.size(); ++j) {
      for (int k = 0; k < valid_ix.size(); ++k) {
        cor_mat(j, k) = run_cors[nums[valid_ix[j] - 1][valid_ix[k] - 1]][i];
      }
    }
    result.push_back(
      List::create(
        Named("valid.ids") = valid_ix,
        Named("cor.mat") = cor_mat
      )
    );
  }
  return wrap(result);
}


/* binary search in vector */
int binary_search_vector(const std::vector<double>& v,
                         const double value) {
    int from = 0;
    int to = v.size();
    while (to - from > 1) {
        int mid = (from + to) / 2;
        if (v[mid] > value) to = mid;
        else from = mid;
    }
    return from;
}

/* run_rank */
// [[Rcpp::export]]
NumericVector run_rank(const NumericVector& v,
                       const int window,
                       const double min_value = -1.0,
                       const double max_value = 1.0,
                       const int min_valid = 1) {
    int n = v.size();
    NumericVector result(n, NAN);
    int from = 0;
    std::vector<double> buf;
    for (int i = 0; i < n; ++i) {
        // remove old observation
        if (i - from == window) {
            if (std::isfinite(v[from])) {
                buf.erase(buf.begin() + binary_search_vector(buf, v[from]));
            }
            ++from;
        }
        // add new observation
        if (std::isfinite(v[i])) {
            buf.push_back(v[i]);
            int pos = buf.size(); --pos;
            while (pos > 0 && buf[pos - 1] > buf[pos]) {
                std::swap(buf[pos - 1], buf[pos]);
                --pos;
            }
            if ((int)buf.size() >= min_valid) {
                int first_pos = pos;
                while (first_pos > 0 && buf[first_pos] == buf[first_pos - 1]) --first_pos;
                if (buf.size() == 1) result[i] = (min_value + max_value) / 2.0;
                else {
                    double q = 0.5 / (buf.size() - 1) * (first_pos + pos);
                    result[i] = min_value + q * (max_value - min_value);
                }
            }
        }
    }
    return result;
}

